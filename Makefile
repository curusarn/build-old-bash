build: ./bash3.2
	# testing
	./bash3.2 -version

prompt: ./bash3.2
	env -i PATH="$$PWD:$$PATH" ./bash3.2 --noprofile --norc

clean:
	-rm -rf bash-3.2.57
	-rm bash-3.2.57.tar.gz

./bash3.2: bash-3.2.57/bash
	cp bash-3.2.57/bash ./bash3.2

bash-3.2.57/bash: bash-3.2.57/Makefile
	# if `make` fails due to yacc, run `sudo apt-get install byacc`
	cd bash-3.2.57 && make || echo 'if `make` fails due to yacc, run `sudo apt-get install byacc`'

bash-3.2.57/Makefile: bash-3.2.57/configure
	cd bash-3.2.57 && ./configure

bash-3.2.57/configure: bash-3.2.57.tar.gz
	tar xvzf bash-3.2.57.tar.gz

bash-3.2.57.tar.gz:
	wget http://ftp.gnu.org/gnu/bash/bash-3.2.57.tar.gz
